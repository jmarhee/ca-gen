#!/bin/bash

function generate_registry_keys {
  echo "Creating certs for $@..." && \
  openssl genrsa -out certs/$@.key 2048 && \
  openssl req -subj "/C=US/ST=NY/L=Flavortown/O=Guy Fieri/OU=Development/CN=$@" -new -key certs/$@.key -out certs/$@.csr && \
  openssl x509 -req -in certs/$@.csr -CA certs/ca.crt -CAkey certs/ca.key -CAcreateserial -out certs/$@.crt -days 500 -sha256
}

if [ ! -f certs/registry.key ]; then
  type openssl >/dev/null 2>&1 || { echo >&2 "OpenSSL is required on your local machine to generate the CA."; exit 1; }
  generate_registry_keys $@
else
  echo -e "Registry cert present. Not replacing."
fi
