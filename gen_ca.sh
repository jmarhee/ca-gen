#!/bin/bash

function generate_CA {
  openssl genrsa -out certs/ca.key 2048 && openssl req -subj "/C=US/ST=NY/L=Flavortown/O=Guy Fieri/OU=Development CA" -config /usr/lib/ssl/openssl.cnf -new -key certs/ca.key -x509 -days 1825 -out certs/ca.crt
}

if [ ! -f ca.key ]; then

  type openssl >/dev/null 2>&1 || { echo >&2 "OpenSSL is required on your local machine to generate the CA."; exit 1; }

  echo "Generating CA certificate in `pwd`/ca.crt..." && \
  generate_CA
else
    echo "CA key and cert already exist. Skipping..."
fi
